#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <semaphore.h>


#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "so_game_protocol.h"
#include "image.h"
#include "surface.h"
#include "world.h"
#include "vehicle.h"
#include "world_viewer.h"
#include "common.h"
#include "funzioni.h"

#define MAX_CLIENT 5
#define max_connessioni 5
#define TCP_port 22500
#define UDP_port 22501

World mondo;
int socket_tcp;
Image* surface_texture;
int socket_udp;
Image* surface_elevation;
Image* vehicle_texture;
int flag;
int cont_thread;
struct sockaddr_in other_addr;

typedef struct thread_args{
		int my_socket_tcp;
}thread_args;

void sig_handler(int signo,siginfo_t* siginfo, void* context){
	if(mondo.vehicles.size==0){ 
		printf("\nCHIUSURA SEREVER\n");
		exit(0);
	}
  flag=1;
  }
  
  
void* gestisci_tcp(void* args){
	int my_socket = ((thread_args*)args)->my_socket_tcp;
	PacketHeader* packet;
	ImagePacket* img_packet;
	ImagePacket* texture;
	IdPacket* esc;
	Vehicle* vehicle_aux;
	Vehicle* v;
	while(!flag){
		packet=RiceviPacchettoTCP(my_socket);
		switch(packet->type){
				case GetTexture:   
							img_packet=(ImagePacket*) packet;
							vehicle_aux=World_getVehicle(&mondo, img_packet->id);
							PacketHeader head;
							ImagePacket* send=(ImagePacket*)malloc(sizeof(ImagePacket));
							head.type=PostTexture;
							head.size=sizeof(ImagePacket);
							send->header=head;
							send->id=img_packet->id;
							send->image=vehicle_aux->texture;
							IdPacket* id = (IdPacket*)RiceviPacchettoTCP(my_socket);
							Vehicle* vec = World_getVehicle(&mondo,id->id);
							InviaTCP((PacketHeader*)send,vec->sockTCP);
							//free(send);
							Packet_free((PacketHeader*)id);
							break;
							
				case VehicleUpdate:
							esc=(IdPacket*) packet;
							printf("Il client:id=%d si è disconnesso\n", esc->id);
							v=World_getVehicle(&mondo, esc->id);
							World_detachVehicle(&mondo, v);
							World_update(&mondo);
							cont_thread--;
							pthread_exit(NULL);
							break;
				default: break;
			}
		Packet_free((PacketHeader*)packet);
	}
	pthread_exit(NULL);
}

void* gestisci_invio_udp(void* args){
	while(1){
		if(mondo.vehicles.size == 0){
		 continue;
		}else{
			PacketHeader* wup = (PacketHeader*)AggiornaNelMondo(&mondo); 
			ListItem* item=mondo.vehicles.first;
			while(item){
				Vehicle* v=(Vehicle*)item;
				InviaUDP(socket_udp, &other_addr,wup);
				item=item->next;
			}
		Packet_free((PacketHeader*)wup);
		}
		usleep(1000);
	}
}

void* gestisci_ricezione_udp(void* args){
	int ret;

	PacketHeader* packet;
	Vehicle* vehicle_aux;
	VehicleUpdatePacket* v_update;
	Vehicle* v;
	int i;
	while(1){
		packet=RiceviPacchettoUDP(socket_udp, &other_addr);
		v_update =(VehicleUpdatePacket*) packet;
		vehicle_aux=World_getVehicle(&mondo, v_update->id);
		if(vehicle_aux == NULL) continue;
		
		
		
		if(vehicle_aux->flag_sockaddr == 0) {
			vehicle_aux->flag_sockaddr = 1;
		}
		vehicle_aux->translational_force_update=v_update->translational_force;
		vehicle_aux->rotational_force_update=v_update->rotational_force;
		vehicle_aux->x=v_update->x;
		vehicle_aux->y=v_update->y;
		vehicle_aux->theta=v_update->theta;
		World_update(&mondo);  				

		Packet_free((PacketHeader*)packet);
	}
	pthread_exit(NULL);
}

Image* AvviaClient(int sock_client, IdPacket* pacchetto_id, Image* v_texture){
	int ret;
  InviaTCP((PacketHeader*) pacchetto_id, sock_client);
  Image* clientTexture;
  PacketHeader* pack;
  ImagePacket* clientImage;
  PacketHeader head;
  
	pack = RiceviPacchettoTCP(sock_client);
	printf("RICEVUTO TEXTURE\n"); 
  clientImage = (ImagePacket*) pack;
	
	clientTexture=clientImage->image;
	
	ImagePacket* mapElevation=(ImagePacket*) RiceviPacchettoTCP(sock_client);
  ImagePacket* send_map = (ImagePacket*)malloc(sizeof(ImagePacket));
  head.type = PostElevation;
  head.size = sizeof(ImagePacket);
	send_map->header = head;
  send_map->id=0;
  send_map->image=surface_elevation;
  InviaTCP((PacketHeader*) send_map, sock_client);

  ImagePacket* mapTexture=(ImagePacket*) RiceviPacchettoTCP(sock_client);
  mapTexture->header = head;
  mapTexture->id=0;
  mapTexture->image=surface_texture;
  InviaTCP((PacketHeader*) mapTexture, sock_client);
	
  return clientTexture;
}


int main(int argc, char **argv) {
  if (argc<3) {
    printf("usage: %s <elevation_image> <texture_image>\n", argv[1]);
    exit(-1);
  }
  char* elevation_filename=argv[1];
  char* texture_filename=argv[2];
  char* vehicle_texture_filename="./images/arrow-right.ppm";
  printf("loading elevation image from %s ... ", elevation_filename);

  // load the images
  surface_elevation = Image_load(elevation_filename);
  if (surface_elevation) {
    printf("Done! \n");
  } else {
    printf("Fail! \n");
  }


  printf("loading texture image from %s ... ", texture_filename);
  surface_texture = Image_load(texture_filename);
  if (surface_texture) {
    printf("Done! \n");
  } else {
    printf("Fail! \n");
  }

  printf("loading vehicle texture (default) from %s ... ", vehicle_texture_filename);
  vehicle_texture = Image_load(vehicle_texture_filename);
  if (vehicle_texture) {
    printf("Done! \n");
  } else {
    printf("Fail! \n");
  }
  
  struct sigaction act ;
  memset(&act,0,sizeof(struct sigaction));
  act.sa_flags = SA_SIGINFO;
  act.sa_sigaction = sig_handler;
  int res = sigaction(SIGINT,&act,NULL);
  if(res){
    return 0;
  }
  
  World_init(&mondo,surface_elevation,surface_texture,1,1,1);
  pthread_t udp_invio,udp_ricezione,tcp[MAX_CLIENT];
  cont_thread = 0;
  int ret;
  
  socket_udp=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	ERROR_HELPER(socket_udp, "Errore socket udp");

	//struct sockaddr_in addr;
	other_addr.sin_family = AF_INET;
	other_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	other_addr.sin_port = htons(UDP_port);

	ret=bind(socket_udp, (struct sockaddr*) &other_addr, sizeof(other_addr));
	ERROR_HELPER(ret, "Errore bind udp");
  
  ret=pthread_create(&udp_invio,NULL,gestisci_invio_udp,NULL);
  PTHREAD_ERROR_HELPER(ret,"Errore creazione thread udp");
  
  ret=pthread_create(&udp_ricezione,NULL,gestisci_ricezione_udp,NULL);
  PTHREAD_ERROR_HELPER(ret,"Errore creazione thread udp");

	socket_tcp=socket(AF_INET,SOCK_STREAM,0);
	ERROR_HELPER(socket_tcp,"Errore creazione socket");
	
	struct sockaddr_in server_addr;
  server_addr.sin_family = AF_INET;
  server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
  server_addr.sin_port = htons(TCP_port);
	
	int size_sock=sizeof(struct sockaddr_in);
  ret=bind(socket_tcp, (struct sockaddr*) &server_addr, size_sock);
  ERROR_HELPER(ret, "Errore bind");

  ret=listen(socket_tcp, max_connessioni);
  ERROR_HELPER(ret, "Errore listen");
  
  int client_sock;
  struct sockaddr_in *client_addr = calloc(1, sizeof(struct sockaddr_in));
  
	Vehicle* vehicle;
	int id=1;
	flag=0;
	printf("PARTITO!\n");
	while(1){
		client_sock=accept(socket_tcp, (struct sockaddr*)client_addr, (socklen_t*) &size_sock);
		if(client_sock<0 && flag) break;
	  ERROR_HELPER(client_sock, "Errore accept");
	  IdPacket* pacchetto_id=(IdPacket*) RiceviPacchettoTCP(client_sock);

    pacchetto_id->id=id;
	  Image* clientTexture=AvviaClient(client_sock, pacchetto_id, vehicle_texture); 
		thread_args args;
		args.my_socket_tcp = client_sock;
		ret=pthread_create(&tcp[cont_thread],NULL,gestisci_tcp,(void*)&args);
		PTHREAD_ERROR_HELPER(ret,"Errore creazione thread tcp");
		vehicle=(Vehicle*) malloc(sizeof(Vehicle));
		Vehicle_init(vehicle, &mondo, pacchetto_id->id, clientTexture);
		vehicle->sockTCP = client_sock;
		printf("Giocatore con id:%d è presente nel mondo\n",pacchetto_id->id);
		World_addVehicle(&mondo, vehicle);
	
		//resetto
		client_addr = calloc(1, sizeof(struct sockaddr_in));
		id++;
		cont_thread++;
		Packet_free((PacketHeader*)pacchetto_id);
	}
	int i;
	for(i=0;i<cont_thread;i++){
		ret=pthread_join(tcp[i],NULL);
		ERROR_HELPER(ret, "Errore terminazione thread");
	}
	
	ret=pthread_join(udp_invio,NULL);
	ret=pthread_join(udp_ricezione,NULL);
	ERROR_HELPER(ret, "Errore terminazione thread");
	
	World_destroy(&mondo);
  return 0;             
}
