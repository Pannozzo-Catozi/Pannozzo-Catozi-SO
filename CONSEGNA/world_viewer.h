#pragma once
#include "world.h"
#include "vehicle.h"

// call this to start the visualization of the stuff.
// This will block the program, and terminate when pressing esc on the viewport
typedef enum ViewType {Inside, Outside, Global} ViewType;

typedef struct WorldViewer{
  World* world;
  float zoom;
  float camera_z;
  int window_width, window_height;
  Vehicle* self;
  ViewType view_type;
} WorldViewer;

void WorldViewer_runGlobal(World* world,
			   Vehicle* self,
			   int* argc, char** argv,int sock,int id);
void WorldViewer_draw(WorldViewer* viewer);

void WorldViewer_destroy(WorldViewer* viewer);

void WorldViewer_reshapeViewport(WorldViewer* viewer, int width, int height);

void specialInput(int key, int x, int y);

void keyPressed(unsigned char key, int x, int y);
