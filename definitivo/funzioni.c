#include "funzioni.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "world.h"

WorldUpdatePacket* AggiornaNelMondo(World* w){
	PacketHeader h;
	h.type = WorldUpdate;
  h.size = sizeof(WorldUpdate);
	ClientUpdate* c =(ClientUpdate*) malloc(sizeof(ClientUpdate)*(w->vehicles.size));

	ListItem* item=w->vehicles.first;
	int i=0;
	//devo aggiornare tutto
  for(int i=0;item;i++){
    Vehicle* v=(Vehicle*)item;
    c[i].id=v->id;
    c[i].x =v->x;
    c[i].y =v->y;
    c[i].theta =v->theta;
    item=item->next;

 	}
	//update da mandare 
 	WorldUpdatePacket* p = (WorldUpdatePacket*)malloc(sizeof(WorldUpdatePacket));
 	p->header = h;
 	p->num_vehicles = w->vehicles.size;
 	p->updates = c;
  return p;
}

void InviaUDP(int sock,struct sockaddr_in* sockaddr,PacketHeader* pack){
  char buf[BUFUDP],size[4];
  int sended=0,tot,ret;
  tot = Packet_serialize(buf,pack);
  while(sended<tot){
    ret = sendto(sock,buf+sended,BUFUDP,0,(struct sockaddr*)sockaddr,sizeof(struct sockaddr_in));
    if(ret<0){
      perror("CLIENT:Errore invio UDP");
      return;
    }
    sended+=ret;
  }
  int* s = (int*)size;
    *s = tot;
    ret = sendto(sock,size,4,0,(struct sockaddr*)sockaddr,sizeof(struct sockaddr_in));
    if(ret<0){
            perror("CLIENT:Errore invio UDP SIZE");
            return;
        }
    return;
}

PacketHeader* RiceviPacchettoUDP(int sock,struct sockaddr_in* sockaddr){
  char buf[BUFUDP],size[4];
  int ret,recv=0;
  int dim = sizeof(struct sockaddr_in);
  while(recv<BUFUDP){
    ret = recvfrom(sock,buf,BUFUDP,0,(struct sockaddr*)sockaddr,(socklen_t*)&dim);
    if(ret<0){
      fprintf(stderr,"CLIENT:Errore ricezione UDP");
      return NULL;
    }
    recv+=ret;
  }
  ret = recvfrom(sock,size,4,0,(struct sockaddr*)sockaddr,(socklen_t*)&dim);
  if(ret<0){
    perror("CLIENT:Errore ricezione UDP SIZE");
    return NULL;
  }
  int* s = (int*)size;
  PacketHeader* re = Packet_deserialize(buf,*s);
  
  return re;
}

void InviaTCP(PacketHeader* packet, int sock){
	char buf[BUF],size[4];
    int tot,sended=0,ret;
    tot = Packet_serialize(buf,packet);
    while(sended<tot){
        ret = send(sock,buf+sended,BUF,0);
        if(ret<0){
            perror("CLIENT:Errore invio TCP");
            return;
        }
        sended += ret;
    }
    int* s = (int*)size;
    *s = tot;
    ret = send(sock,size,4,0);
    if(ret<0){
            perror("CLIENT:Errore invio TCP SIZE");
            return;
        }
    return;
}

PacketHeader* RiceviPacchettoTCP(int sock){
	char buf[BUF],size[4];
	int ret;
  ret = recv(sock,buf,BUF,MSG_WAITALL);
  if(ret<0){
    perror("CLIENT:Errore ricezione TCP");
    return NULL;
  }
  ret = recv(sock,size,4,MSG_WAITALL);
  if(ret<0){
    perror("CLIENT:Errore ricezione TCP SIZE");
    return NULL;
  }
  int* s = (int*)size;
  return Packet_deserialize(buf,*s);
}  

